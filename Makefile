all:
	glib-compile-schemas --strict --targetdir . .
	glib-compile-resources --sourcedir . --internal --generate --c-name test --target=test-resources.hh test.gresource.xml
	glib-compile-resources --sourcedir . --internal --generate --c-name test --target=test-resources.cc test.gresource.xml
	g++ -o test test.cc test-resources.hh test-resources.cc $(shell pkg-config --cflags --libs glib-2.0 gio-2.0) -std=gnu++2a -ldl

clean:
	-rm -f gschemas.compiled test-resources.hh test-resources.cc test
