/*
 * Copyright © 2020 Christian Persch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <dlfcn.h>
#include <sys/mman.h>

#include <glib.h>
#include <gio/gio.h>

#include <cassert>
#include <memory>

template<typename T>
class Deleter;

template<typename T>
using Freeable = std::unique_ptr<T, Deleter<T>>;

template<typename T>
auto take_freeable(T* t) { return Freeable<T>{t}; }

#define FREEABLE(T, func) \
template<> \
class Deleter<T> { \
public: inline void operator()(T* t) { func(t); } \
};

FREEABLE(GBytes, g_bytes_unref)
FREEABLE(GSettings, g_object_unref)
FREEABLE(GSettingsSchema, g_settings_schema_unref)
FREEABLE(GSettingsSchemaSource, g_settings_schema_source_unref)
FREEABLE(char, g_free)

/* We want to load a schema from a built-in compiled schema source.
 * However, glib offers no public API to do so (see
 * https://gitlab.gnome.org/GNOME/glib/-/issues/499).
 *
 * This is the simplest and 'least' hacky work-around I could think of.
 * Using the knowledge of how g_settings_schema_source_new_from_directory()
 * works internally by using g_mapped_file_new() on the gschemas.compiled
 * file in the passed directory, we interpose g_mapped_file_new(), and when
 * passed the correct (fake) file path, instead of creating the mapped
 * file normally, we instead load the gschemas.compiled from resources,
 * create a memfd, write the data to it, and then return
 * g_mapped_file_new_from_fd() on the memfd.
 *
 * Then we can use g_settings_schema_source_lookup() to find our schema,
 * and g_settings_new_full() to create a GSettings for our build-in schema.
 */

#define GSCHEMAS_DOT_COMPILED "gschemas.compiled"
#define SCHEMA_DIR "/ce10b993-c52a-4bde-a1c8-89f2b7b1dd83"
#define SCHEMA_FILE SCHEMA_DIR G_DIR_SEPARATOR_S GSCHEMAS_DOT_COMPILED
#define RESOURCE_PATH "/org/example/chpe"
#define SCHEMA_RESOURCE_PATH RESOURCE_PATH "/schemas"

using mapped_file_new_type = decltype(&g_mapped_file_new);

extern GMappedFile* g_mapped_file_new(char const* filename,
                                      gboolean writable,
                                      GError** error);

GMappedFile*
g_mapped_file_new(char const* filename,
                  gboolean writable,
                  GError** error)
{
  static auto original = mapped_file_new_type{nullptr};
  if (!original)
    original = mapped_file_new_type(dlsym(RTLD_NEXT, "g_mapped_file_new"));
  assert(original != nullptr);

  if (!g_str_equal(filename, SCHEMA_FILE))
    return original(filename, writable, error);

  auto data = take_freeable(g_resources_lookup_data(SCHEMA_RESOURCE_PATH,
                                                    GResourceLookupFlags(0),
                                                    error));
  if (!data)
    return nullptr;

  auto const fd = memfd_create(GSCHEMAS_DOT_COMPILED, MFD_CLOEXEC);
  if (fd == -1) {
    auto const errsv = errno;
    g_set_error(error, G_IO_ERROR, g_io_error_from_errno(errsv),
                "memfd_create failed: %s", g_strerror(errsv));
    return nullptr;
  }

  auto const r = write(fd, g_bytes_get_data(data.get(), nullptr), g_bytes_get_size(data.get()));
  if (r < g_bytes_get_size(data.get())) {
    auto const errsv = errno;
    g_set_error(error, G_IO_ERROR, G_IO_ERROR_FAILED,
                "Failed to write all data to memfd");
    return nullptr;
  }

  return g_mapped_file_new_from_fd(fd, writable, error);
}

#define SCHEMA_ID "org.example.chpe.Test"

int
main (int argc,
      char *argv[])
{
  GError* err = nullptr;
  auto schema_source = take_freeable(g_settings_schema_source_new_from_directory(SCHEMA_DIR, nullptr, true, &err));
  g_assert_no_error(err);
  assert(schema_source);

  auto schema = take_freeable(g_settings_schema_source_lookup(schema_source.get(), SCHEMA_ID, false));
  assert(schema);

  auto settings = take_freeable(g_settings_new_full(schema.get(), nullptr, nullptr));
  assert(settings);

  auto str = take_freeable(g_settings_get_string(settings.get(), "foo"));
  assert(str);
  g_print("Foo: %s\n", str.get());

  return 0;
}
